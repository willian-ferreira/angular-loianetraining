# Módulo => Data Binding

## Interpolação:  {{valor}}
- Pegar o valor de um método e fazer a saida dele no template

## Property Binding: [prop]="valor"
- Conseguimos ter a saída do component
- Valor do Component para o Template
- Usa-se conchetes ``` <img [src]="urlImg"/> ```
- Formado padrão é através de bind-nomePropriedade ``` <img bind-src="urlImg"/> ```
- Quando não existe uma propriedade no elemento usa-se ``` [attr.colspan] ``` (Dificilmente terá essas situações)

## Evento: (evento)="handler"
- Escutas eventos do template como click, foco em campo, e executar um metodo do component para executar uma logica

## Two Way Data Binding: [(ngModel)]="propriedade"
- conseguimos manter template e component atualizados



