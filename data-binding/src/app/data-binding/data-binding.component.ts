import { Component, OnInit } from '@angular/core';
import { FormsModule, NgModel } from '@angular/forms';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
  /* o CSS pode ser usado assim:
  styles: [`
    .highlight {
      background-color: yellow;
      font-weight: bold; 
    }`
  ] */
})
export class DataBindingComponent implements OnInit {

  url: any = 'http://loiane.com';
  cursoAngular: boolean = true;
  urlImagem: any = 'https://placeimg.com/400/200/any';
  valorAtual: string = '';
  valorSalvo = '';
  isMouseOver: boolean = false;
  nomeDoCurso: string = 'Angular';
  valorInicial = 15;
  
  constructor() { }

  ngOnInit() { }

  getValor() {
    return 1;
  }

  getCurtirCurso() {
    return true;
  }

  botaoClicado() {
    alert("Botão clicado");
  }

  onKeyUp(evento: KeyboardEvent) {
    // console.log((<HTMLInputElement>evento.target).value);
    this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  salvarValor(valor) {
    this.valorSalvo = valor;
  }

  onMouseOverOut() {
    this.isMouseOver = !this.isMouseOver;
  }

  onMudouValor(evento) {
    console.log(evento.novoValor);
  }

}
