Loiane ou alguém se poder me tirar uma dúvida, neste caso ai tudo ok com bootstrap, mas estou usando o materialize e, neste caso é um pouco diferente a forma dos icons ex
<i class="material-icons">thumb_down</i>......como podem ver o nome da figura vai entre as tags no caso "'thumb_down'"
Neste caso além de criar a variavel meuFavorito tbm criei icon que recebe o nome de 'thumb_down'
na minha função usei uma lógica não muito inteligente, muitas linhas mas foi o que consegui fazer ex 

meuFavorito: boolean = false;
icon: string = 'thumb_down';

onClick(){
    this.meuFavorito = !this.meuFavorito;
    if(this.meuFavorito){
      this.icon = 'thumb_up';
    }else{
      this.icon = 'thumb_down';
    }
  }

logo no template fiz assim :
 
<ul style="list-style: none">
  <li *ngFor="let produto of produtos, let i = index">
    {{ i + 1}} - {{ produto }} <i class="material-icons"
  [??????]=??????
    (click)="onClick()"
    >{{ icon }}</i>
  </li>
</ul>


Usa interpolação, amigo. <i class="material-icons">favorite{{ !meuFavorito ? "_border" : "" }}</i>﻿


o que eu fiz , criei duas classes no diretiva-ngclass.css 

.fav::before{content: 'star'}
.not-fav::before{content: 'star_border' }

e no diretiva-ngclass.html

<i class="material-icons"

  [ngClass]="{'fav': meuFavorito, 'not-fav': !meuFavorito}"

  (click)="onClick()">
</i>﻿