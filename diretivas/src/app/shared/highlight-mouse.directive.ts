import { Directive, HostListener, ElementRef, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[highlightMouse]'
})
export class HighlightMouseDirective {

  @HostListener('mouseenter') passarMouse(){
    // this._renderer.setElementStyle(
    //   this._elementRef.nativeElement,
    //   'backgroundColor', 'blue'
    // );
    this.backgroundColor = 'red';
  }

  @HostListener('mouseleave') tirarMouse(){
    // this._renderer.setElementStyle(
    //   this._elementRef.nativeElement,
    //   'backgroundColor', 'white'
    // );
    this.backgroundColor = 'white';
  }

  @HostBinding('style.backgroundColor') backgroundColor: String;

  constructor(
    private _elementRef: ElementRef,
    private _renderer: Renderer
  ) { }

}
