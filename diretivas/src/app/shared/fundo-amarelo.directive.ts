import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  selector: '[appFundoAmarelo]'
  // Se quiser que essa direvitiva seja aplica a um elemento, especificar antes como aqui:
  // selector: 'button[appFundoAmarelo]'
})
export class FundoAmareloDirective {

  constructor(
    private _elementRef: ElementRef,
    private _renderer: Renderer
  ) { 
    console.log(this._elementRef);
    // Evitar usar, por segurança
    // this._elementRed.nativeElement.style.backgroundColor = 'yellow';
    // Usar então
    this._renderer.setElementStyle(this._elementRef.nativeElement, 'background-color', 'yellow');
    // Esse comando faz acima faz a mesma coisa que o anterior, porem de forma segura
  }

}
