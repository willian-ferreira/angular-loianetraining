import { Component, OnInit } from '@angular/core';

import { CursosService } from './cursos.service';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css'],
  // Se quiser usar apenas em um component
  // providers: [CursosService]
})
export class CursosComponent implements OnInit {

  // cursos: string[] = ['Angular 2', 'Java', 'Phonegap'];
  cursos: string[] = [];
  
  // Normal do JS
  // cursoService: CursosService;

  constructor(private cursoService: CursosService) { 
    // Normal do JS
    // this.cursoService = new CursosService();

    // this.cursoService = _cursoService
  }

  ngOnInit() {
    this.cursos = this.cursoService.getCursos();
  }

}
