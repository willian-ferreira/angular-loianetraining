import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
	selector: 'app-template-form',
	templateUrl: './template-form.component.html',
	styleUrls: ['./template-form.component.css']
})
export class TemplateFormComponent implements OnInit {

	usuario: any = {
		nome: null,
		email: null
	};
	private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

  	constructor(private http: HttpClient) { }

	ngOnInit() { }

	onSubmit(form) {
		console.log(form);
		// console.log(this.usuario);
		this.http.post('https://httpbin.org/post', JSON.stringify(form.value), this.options)
		// .map(resp => resp)
		.subscribe(dados => {
			console.log(dados);
			form.form.reset();
		});
	}

	verificaValidTouched(campo) {
		return !campo.valid && campo.touched;
	}

	aplicaCssErro(campo) {
		return {
			'has-error': this.verificaValidTouched(campo)
		}
	}

	consultar(cep, form) {
		// Nova variável "cep" somente com dígitos.
		cep = cep.replace(/\D/g, '');

		// Verifica se campo cep possui valor informado.
		if (cep !== '') {
			// Expressão regular para validar o CEP.
			const validacep = /^[0-9]{8}$/;

			// Valida o formato do CEP.
			if (validacep.test(cep)) {

				this.http.get(`//viacep.com.br/ws/${cep}/json`)
					// .map(dados => dados.json())
					.subscribe(dados => this.popularDadosForm(dados, form));
			}
		}
	}

	popularDadosForm(dados, form) {
		/* form.setValue({
			nome: form.value.nome,
			email: form.value.email,
			endereco: {
				rua: dados.logradouro,
				cep: dados.cep,
				numero: '',
				complemento: dados.complemento,
				bairro: dados.bairro,
				cidade: dados.localidade,
				estado: dados.uf
			}
		}); */

		form.form.patchValue({
			endereco: {
				rua: dados.logradouro,
				cep: dados.cep,
				complemento: dados.complemento,
				bairro: dados.bairro,
				cidade: dados.localidade,
				estado: dados.uf
			}
		});
	}
}
