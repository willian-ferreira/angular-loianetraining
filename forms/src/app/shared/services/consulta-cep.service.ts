import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ConsultaCepService {

	constructor(private http: HttpClient) { }
	
	consultarCep(cep: string): Observable<object> {
		cep = cep.replace(/\D/g, '');

		if (cep !== '') {

			const validacep = /^[0-9]{8}$/;

			if (validacep.test(cep)) {
	
				return this.http.get(`//viacep.com.br/ws/${cep}/json`);

			}
		}
	}
}
