import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { TemplateFormModule } from './template-form/template-form.module';
import { DataFormModule } from './data-form/data-form.module';

@NgModule({
    declarations: [
      	AppComponent,
    ],
    imports: [
        BrowserModule,
		AppRoutingModule,
        FormsModule,
        TemplateFormModule,
        DataFormModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
