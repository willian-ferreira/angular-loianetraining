```typescript
// Faz uma instancia do formGroup que recebe um array como parametros
/* this.formulario = new FormGroup({
    nome: new FormControl(null),
    email: new FormControl(null),
    endereco: new FormGroup({
        cep: new FormControl() ...
    })
}); */

this.formulario = this.formBuilder.group({
    nome: [null, Validators.required],
    email: [null, [Validators.required, Validators.email]],
    endereco: this.formBuilder.group({
        cep: [null, Validators.required],
        numero: [null],
        complemento: [null, Validators.required],
        rua: [null, Validators.required],
        bairro: [null, Validators.required],
        cidade: [null, Validators.required],
        estado: [null, Validators.required]
    })
});
```