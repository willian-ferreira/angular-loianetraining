import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlunosService } from '../alunos.service';
import { FormDeActivated } from '../../guards/form-deactivated';


@Component({
    selector: 'app-aluno-form',
    templateUrl: './aluno-form.component.html',
    styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements OnInit, OnDestroy, FormDeActivated{
	
	id: number;
	inscricao: Subscription;
	aluno: any;

	private formMudou: boolean = false;

	constructor(
		private route: ActivatedRoute,
		private service: AlunosService,
		private router: Router
	) { }

	ngOnInit() {
		this.inscricao = this.route.params.subscribe((params: any) => {
			this.id = params['id'];

			this.aluno = this.service.getAluno(this.id);

			if (this.aluno == null) {
				this.aluno = {};
			}
		});
	}

	ngOnDestroy() {
		this.inscricao.unsubscribe();
	}

	onInput() {
		this.formMudou = true;
	}

	podeMudarRota() {
		
		if (this.formMudou) {
			confirm('Tem certeza que deseja sair?');
		}

		return true;
	}

	podeDesativar() {
		this.podeMudarRota();
	}
}
