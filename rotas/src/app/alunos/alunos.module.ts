import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlunosRoutingModule } from './alunos-routing.module';
import { AlunosComponent } from './alunos.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunosService } from './alunos.service';
import { FormsModule } from '@angular/forms';
import { AlunosDeactivatedGuard } from '../guards/alunos-deactivated.guard';
import { ResolverGuard } from './guards/resolver.guard';

@NgModule({
	imports: [
		CommonModule,
		AlunosRoutingModule,
		FormsModule
	],
	declarations: [
		AlunosComponent,
		AlunoFormComponent,
		AlunoDetalheComponent
	],
	providers: [
		AlunosService,
		AlunosDeactivatedGuard,
		ResolverGuard
	]
})
export class AlunosModule { }
