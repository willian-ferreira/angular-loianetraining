import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlunosService } from '../alunos.service';
import { Aluno } from '../aluno';

@Component({
    selector: 'app-aluno-detalhe',
    templateUrl: './aluno-detalhe.component.html',
    styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit, OnDestroy {

    id: number;
    inscricao: Subscription;
    aluno: Aluno;

	constructor(
		private route: ActivatedRoute,
		private service: AlunosService, 
		private router: Router
	) { }

    ngOnInit() {
		/* this.inscricao = this.route.params.subscribe((params: any) => {
			this.id = params['id'];

			this.aluno = this.service.getAluno(this.id);

			if (this.aluno == null) {
				console.log('nao existe');
				// this.router.navigate(['nao-encontrado']);
			}
		}); */
		this.inscricao = this.route.data.subscribe(
			(info: {aluno : Aluno}) => {
				console.log(info);
				this.aluno = info.aluno;
			} 
		);
	}
	
	ngOnDestroy() {
		this.inscricao.unsubscribe();
	}

	editarAluno() {
		this.router.navigate(['/alunos', this.aluno.id, 'editar']);
	}

}
