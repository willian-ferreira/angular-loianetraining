import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';

// import { AlunoFormComponent } from '../alunos/aluno-form/aluno-form.component';
import { FormDeActivated } from './form-deactivated';

@Injectable({
  	providedIn: 'root'
})
// export class AlunosDeactivatedGuard implements CanDeactivate<AlunoFormComponent> {

// Para ser reutilzado mais vezes passamos a interface
export class AlunosDeactivatedGuard implements CanDeactivate<FormDeActivated> {
	canDeactivate(
		component: FormDeActivated,
		next: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
		

		return component.podeDesativar();
		// return component.podeMudarRota();
	}
}
