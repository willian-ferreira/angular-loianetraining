# Curso de Angular da Loiane Groner

- Introdução :heavy_check_mark:
- Componentes e Templantes :heavy_check_mark:
- Data Binding :heavy_check_mark:
- Diretivas :heavy_check_mark:
- Serviços :heavy_check_mark:
- Formulários :heavy_check_mark:
- Roteamento :heavy_check_mark:
- Integração com o Servidor
-  CRUD Mestre - Detalhe

![Blocos Principais](https://bloghugocoutinho.files.wordpress.com/2017/12/angularblocos.png?w=1060)

### Escolhendo o pré-processador

```
$ ng new meu-projeto --style=sass

$ ng new meu-projeto --style=less

$ ng new meu-projeto --style=stylus
```

Caso ja tenha um projeto

```
$ ng set defaults.styleExt scss

$ ng set defaults.styleExt less

$ ng set defaults.styleExt styl
```

Porém se ja tiver iniciado o projeto, os arquivos que já tinham extensão .css vão continuar, e terão que ser mudados na mão, 
já os novos components ja vão vir com arquivos .scss, etc.

### ng lint, ng test, ng e2e

ng lint: Vai escanear o código, verificar as boas práticas d style guide, se tem algo fanto, como ";". Procura por erros.

ng test: Funciona com jasmine. Faz testes unitários

ng e2e: testes de end 2 end, ultiliza o karma. 

### Gerar o build de produção

```
$ cd nome-projeto

$ ng build --target=development --enviroment=dev

$ ng build --dev --e=dev

$ ng build --dev

$ ng buid
```
Vai ser gerado dentro do dist 

- Será gerado um index.html que adiciona só os imports do inline.js, main.bundle.js e polyfills.bundle.js
- Arquivo main.bundle.js contém todo projeto + HTML5 + CSS3 (legível)
- polyfills.bundle.js ajuda o brownser a renderizar o código
- Útil para integração com backend (PHP, JAVA, Python, etc)
- Código que dá pra debugar

Build de produção gera um arquivo min, caso tenha uma variável com o nome "variável" ela vai se chamar "a"

### Components 

Os component são responsáveis pela exibição dos dados.

### Diretivas

- ***Diretivas estruturais =>*** Interagem com a view e modificam a estrutura do DOM e/ou código HTML
    - *ngFor
    - *ngIf

- ***Diretivas de atributos =>*** Interagem com o elemento em que foram aplicadas
    - ng-class
    - ng-style

Diretiva ngIf

Funciona como: 

``` javascript
var condicao []

if(condicao.length > 0) {
    // Lógica...
} else {
    // Outra Lógica...
}
```

Diretiva ngSwitch, ngSwitchCase e ngSwitchDefault

Funciona como: 

``` javascript
var viewMode = 'mapa';

switch (viewMode) {
    case 'mapa': // Lógica, caso for mapa...;
        break;
    case 'lista': // Lógica, caso for lista...;
        break;
    default: // Lógica padrão ...
}
```

Diretiva ngFor

Funciona como: 

``` javascript
for(let i = 0; i < cursos.length; i++) {
    let curso = cursos[i]
}
```

### Por que usamos o "*" nas diretivas

Por trás dos planos o angular cria um template, usa o property binding do ng if, dessa maneira:

``` html
<template [ngIf]="mostrarCursos">
    <div>Lista de cursos aqui</div>
<template>    
```

Funciona também:

``` html
<div template="ngIf mostrarCursos">
    Lista de cursos aqui
</div>
```
 ### Serviços

 Os services, são responsáveis por terem toda lógica de negócio da aplicação. Podem ser globais
 ou por components, caso usar mais de uma vez, declarar como global, como services de formatação etc, já
 se for usar apenas uma vez declarar no component, como um crud apenas para aquele component único. 

### Site para requisições

- POST: https://resttesttest.com
- CEP: http://viacep.com.br/
